/**
 * 
 * Configure jest
 * 
 */

/**
 * 
 * Provide a test environment for mongo using an in-memory mongo instance
 * 
 */
export const preset = '@shelf/jest-mongodb';
export const testEnvironment = 'node';
