FROM node:12.16.1-alpine AS development

RUN apk upgrade
RUN apk add --update bash
RUN apk add --update alpine-sdk
RUN mkdir /app
WORKDIR /app
COPY . /app
ENV NODE_ENV=development
RUN npm install
RUN npm run build


FROM node:12.16.1-alpine AS production

RUN mkdir /app
WORKDIR /app
COPY --from=development /app/dist /app/dist
COPY --from=development /app/package.json /app
COPY --from=development /app/package-lock.json /app
ENV NODE_ENV=production
RUN npm install
CMD node ./dist/main
