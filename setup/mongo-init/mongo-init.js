/**
 *
 * Creates the openscn database, the openscn-edge-components collection and
 * the openscn user
 *
 */

conn = new Mongo();
db = conn.getDB("openscn");
db.createCollection('edge-components');
db.createUser({
  user: 'openscn',
  pwd: 'openscn',
  roles: [
    {
      role: 'readWrite',
      db: 'openscn'
    }
  ]
});
