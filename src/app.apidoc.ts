// common apidoc blocks

/**
 *
 * @apiDefine apiTokenHeader
 *
 * @apiHeader {String} x-web-api-token <token>
 *
 * @apiHeaderExample {json} Authorized
 * {
 *  "x-management-api-token": "a big and super secret string"
 * }
 *
 */

/**
 *
 * @apiDefine notFoundError
 *
 * @apiError (Error - 4xx) {404} NotFoundError Not Found Error
 *
 * @apiErrorExample {json} 404 Not Found Example Response
 *  HTTP/1.1 404 Not Found Error
 *  {
 *   "type": "error",
 *   "statusCode": 404,
 *   "message": "The resource was not found"
 *  }
 *
 */

/**
 *
 * @apiDefine internalError
 *
 * @apiError (Error - 5xx) {500} InternalServerError Internal Server Error
 *
 * @apiErrorExample {json} 500  Internal Error Example Response
 *  HTTP/1.1 500 Internal Server Error
 *  {
 *   "type": "error",
 *   "code": 500,
 *   "message": "Internal Server Error"
 *  }
 *
 */

/**
 *
 * @apiDefine notImplementedError
 *
 * @apiError (Error - 5xx) {501} NotImplementedError Not Implemented Error
 *
 * @apiErrorExample {json} 500  Not Implemented Error Example Response
 *  HTTP/1.1 500 Not Implemented Error
 *  {
 *   "type": "error",
 *   "code": 501,
 *   "message": "Not Implemented Error"
 *  }
 *
 */

/**
 *
 * @apiDefine conflictError
 * @apiError (Error - 4xx) {409} Conflict Conflict Error
 *
 * @apiErrorExample {json} 409  Conflict Example Response
 *  HTTP/1.1 409 Conflict
 *  {
 *   "type": "error",
 *   "code": 509,
 *   "message": "Conflict"
 *  }
 *
 */
