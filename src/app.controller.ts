import {
  Controller,
  Get,
  Post,
  Body,
  ForbiddenException,
} from '@nestjs/common';
import { AppService } from './app.service';
import { PostValueDTO } from './app.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return 'alive';
  }

  @Post()
  async postValue(@Body() dto: PostValueDTO) {
    let bundle;
    try {
      bundle = await this.appService.handlePost(dto);
    } catch (err) {
      return 404;
    }

    let isAuthenticated;
    try {
      isAuthenticated = await this.appService.authenticate(
        bundle.edgeComponent,
        bundle.dependentComponent,
      );
      if (!isAuthenticated) {
        throw new ForbiddenException('Not authenticated');
      }
    } catch (err) {
      return 401;
    }

    try {
      await this.appService.postValue(dto);
      return 0;
    } catch (err) {
      return 500;
    }
  }
}
