import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StorageModule } from './lib/storage/storage.module';
import { MongooseModule } from '@nestjs/mongoose';
import { IoTValuesSchema } from './iot.value.schema';

@Module({
  imports: [
    StorageModule,
    MongooseModule.forFeature([
      { name: 'IoTValuesSchema', schema: IoTValuesSchema },
    ]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
