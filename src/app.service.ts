import {
  Injectable,
  NotFoundException,
  ForbiddenException,
  InternalServerErrorException,
} from '@nestjs/common';
import { EdgeComponentsManagementClient } from '@openscn/edge-components-client';
import { PostValueDTO } from './app.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IoTValue } from './iot.value.interface';

@Injectable()
export class AppService {
  private readonly edgeComponentsClient: EdgeComponentsManagementClient;

  constructor(
    @InjectModel('IoTValuesSchema')
    private iotValueModel: Model<IoTValue>,
  ) {
    this.edgeComponentsClient = new EdgeComponentsManagementClient(
      process.env.OPENSCN_EC_HOST,
      process.env.OPENSCN_EC_MNG_TOKEN,
    );
  }

  async handlePost(dto: PostValueDTO) {
    let edgeComponent;
    let dependentComponent;

    try {
      const result = await Promise.all([
        this.edgeComponentsClient.getOneEdgeComponent({
          uuid: dto.edgeComponent,
        }),
        this.edgeComponentsClient.getOneDependentComponent({
          uuid: dto.sensor,
        }),
      ]);

      edgeComponent = result[0];
      dependentComponent = result[1];
    } catch (err) {
      throw err;
    }

    if (!edgeComponent) {
      throw new NotFoundException('Edge component was not found');
    } else if (!dependentComponent) {
      throw new NotFoundException(`Dependent component was not found`);
    }

    return {
      edgeComponent,
      dependentComponent,
    };
  }

  async authenticate(edgeComponent, dependentComponent) {
    const sensorInEdge = edgeComponent.dependentComponents.find(
      item => item.uuid === dependentComponent.uuid,
    );
    if (!sensorInEdge) {
      throw new ForbiddenException(
        `${dependentComponent.uuid} does not belong to edge component ${edgeComponent.uuid}`,
      );
    }
    return sensorInEdge;
  }

  testValueType(dependentComponent, value: string): boolean {
    if (dependentComponent.valueType === 'int') {
      return Number.isInteger(Number(value));
    } else if (dependentComponent.valueType === 'int') {
      return !isNaN(parseFloat(value));
    } else if (dependentComponent.valueType === 'string') {
      return value.length && value.length <= 255;
    } else {
      throw new InternalServerErrorException();
    }
  }

  async postValue(dto: PostValueDTO) {
    const model = new this.iotValueModel({
      sensor: dto.sensor,
      edgeComponent: dto.edgeComponent,
      value: dto.value,
      createdAt: new Date(),
    });

    try {
      await model.save();
      return 0;
    } catch (err) {
      throw err;
    }
  }
}
