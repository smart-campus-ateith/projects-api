import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
const mongod = new MongoMemoryServer();

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: async () => ({
        uri: await mongod.getConnectionString(),
        connectionName: 'openscn-edge-components-registry-test',
      }),
    }),
  ],
  providers: [],
})
export class StorageTestingModule {}

export const closeDatabase = async () => {
  await mongod.stop();
};
